/**
* Movie.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
    schema:true,
    attributes: {
        name: {
          type: 'string', 
          required: true,
          maxLength: 100
        }, 
        email: { 
          type: 'email',
          required: true
        }, 
        enabled: { 
          type: 'boolean',
          defaultsTo: true,
        },
        users:{
          collection: "users",
          via: "company"
        }
    }
};

