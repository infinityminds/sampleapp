/**
* Users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
module.exports = {
    schema:true,

    //tableName: 'user',
    //autoCreatedAt: true,
    //autoUpdatedAt: true,
    //autoPK: true,   
     
    attributes: {
        firstName: {
          type: 'string', 
          required: true,
          maxLength: 100
        },
        lastName: {
          type: 'string', 
          required: true,
          maxLength: 100
        },   
        email: { 
          type: 'email',
          required: true
        },
        company:{
          model: "companies"
        },
        enabled: { 
          type: 'boolean',
          defaultsTo: true,
        } 
        /*,
        roles: {
          collection: 'role',
          via: 'users'
        }*/


    }
};


