 
angular.module('securaApp.controllers',[])

.controller('CompanyListController',function($scope,$state,$window,CompaniesFactory){
    $scope.companies=CompaniesFactory.query();
     
})

.controller('CompanyViewController',function($scope,$state,$stateParams,CompanyFactory,popupService){
    $scope.company=CompanyFactory.get({id:$stateParams.id}); 
 
    $scope.deleteCompany = function (userId) {
    //TODO:Investigar si esto debe o no estar en los controles
        if(popupService.showPopup('desea eliminar esta información?')){
            CompanyFactory.delete({ id: userId.id }, function(){ 
                $state.go('companies'); 
            }); 
        }
    };
})

.controller('CompanyCreateController',function($scope,$state,$stateParams,CompanyFactory){
    $scope.company=new CompanyFactory();
    $scope.company.enabled=true;
    
    $scope.addCompany=function(){
        $scope.company.$save(function(){
            $state.go('companies');
        });
    }
})

.controller('CompanyEditController',function($scope,$state,$stateParams,CompanyFactory){
    $scope.updateCompany=function(){
        $scope.company.$update(function(){
            $state.go('companies');
        });
    };
    $scope.loadCompany=function(){
        $scope.company=CompanyFactory.get({id:$stateParams.id});
    };
    $scope.loadCompany();
});