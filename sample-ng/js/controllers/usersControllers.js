 
angular.module('securaApp.controllers')

.controller('UsersListController',function($scope,$state,$window,UsersFactory){
    $scope.users=UsersFactory.query();
     
})

.controller('UserViewController',function($scope,$state,$stateParams, UserFactory, popupService){
    $scope.user=UserFactory.get({id:$stateParams.id}); 
     $scope.deleteUser = function (userId) {
    //TODO:Investigar si esto debe o no estar en los controles
        if(popupService.showPopup('desea eliminar esta información?')){
            UserFactory.delete({ id: userId.id }, function(){ 
                $state.go('users'); 
            }); 
        }
    };
})

.controller('UserCreateController',function($scope, $state, $stateParams, UserFactory, CompaniesFactory){
    $scope.user=new UserFactory();
    $scope.companies=CompaniesFactory.query();
    $scope.user.enabled=true;
    
    
    $scope.addUser=function(){
        $scope.user.$save(function(){
            $state.go('users');
        });
    }
})

.controller('UserEditController',function($scope,$state,$stateParams, UserFactory, CompaniesFactory, CompanyFactory){
    $scope.companies=CompaniesFactory.query();
    
    $scope.updateUser=function(){
    // $scope.user.company=CompanyFactory.get({id:user.company.id});//cargo la compañia correcta
            //console.log($scope.user);
        $scope.user.$update(function(){
           $state.go('users');
        });
    };

    $scope.loadCompany=function(){
        $scope.user.company=CompanyFactory.get({id:$scope.user.company.id});
    };

    $scope.loadUser=function(){
        $scope.user=UserFactory.get({id:$stateParams.id});
    };
    $scope.loadUser();
});