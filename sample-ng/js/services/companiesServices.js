
var services = angular.module('securaApp.services', ['ngResource']);

services.factory('CompaniesFactory', function ($resource) {
    return $resource('http://localhost:1337/api/companies', {}, {
        query: { method: 'GET', isArray: true },
        create: { method: 'POST' }
    });
    }).service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
 
});

services.factory('CompanyFactory', function ($resource) {
    return $resource('http://localhost:1337/api/companies/:id', {}, { 
        show: { method: 'GET' },
        update: { method: 'PUT', params: {id: '@id'} },
        delete: { method: 'DELETE', params: {id: '@id'} }
    })
});







